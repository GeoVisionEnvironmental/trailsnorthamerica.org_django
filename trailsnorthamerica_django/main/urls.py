from django.conf.urls import patterns, url
from main import views

urlpatterns = [ # = patterns('', -- old patterning style, doesn't work
  url(r'^$', views.index, name='index'),
  url(r'^about/', views.about, name='about'),
  url(r'^register/$', views.register, name='register'),
  url(r'^login/$', views.user_login, name='login'),
]