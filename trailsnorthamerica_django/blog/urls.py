from django.conf.urls import *
from blog.views import blog_display

urlpatterns = patterns('', 
    url(r'^$', blog_display),
)
