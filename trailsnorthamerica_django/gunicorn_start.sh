#!/bin/sh

# starts gunicorn for trailsnorthamerica.org django website

cd /usr/home/pms/Documents/GeoVision_Environmental/TrailsNorthAmerica/trailsnorthamerica.org/websites/trailsnorthamerica.org.django.dev/trailsnorthamerica_django
gunicorn trailsnorthamerica_django.wsgi --bind 127.0.0.1:8093 --workers 8 --daemon
cd /